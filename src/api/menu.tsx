import HomePage from "../pages/Home";
import TodoPage from "../pages/Todo";

export const menu = {
  menuItems: [
    {
      name: "Home",
      path: "/home/",
      element: <HomePage />,
    },
    {
      name: "TodoList",
      path: "/todolist",
      elemebt: <TodoPage />,
    },
  ],
  getMenuItems: () => menu.menuItems,
};
