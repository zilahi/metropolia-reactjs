import { ReactElement } from "react";

function HomePage(): ReactElement {
  return (
    <>
      <h1 style={{ textAlign: "center" }}>Welcome To My Todolist site</h1>
    </>
  );
}

export default HomePage;
