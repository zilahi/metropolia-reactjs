import { useQuery } from "@tanstack/react-query";
import { ReactElement } from "react";

function UserList(): ReactElement {
  const { isLoading, data } = useQuery({
    queryKey: ["users"],
    queryFn: async () => {
      const response = await fetch("https://reqres.in/api/users");
      return response.json();
    },
    refetchOnWindowFocus: false,
  });

  console.log("data", data);
  return (
    <>
      {(isLoading || !data) && <p>Loading...</p>}
      <table>
        <thead>
          <tr>
            <td>Name</td>
            <td>Email</td>
            <td>Image</td>
          </tr>
        </thead>
        {data &&
          Array.isArray(data.data) &&
          data.data.map(
            ({
              id,
              last_name,
              first_name,
              email,
              avatar,
            }: {
              id: number;
              first_name: string;
              last_name: string;
              email: string;
              avatar: string;
            }): ReactElement => (
              <tr key={id}>
                <td>
                  {first_name} {last_name}
                </td>
                <td>{email}</td>
                <td>
                  <img src={avatar} alt="avatar" />
                </td>
              </tr>
            ),
          )}
      </table>
    </>
  );
}

export default UserList;
