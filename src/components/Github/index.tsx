import { useQuery } from "@tanstack/react-query";
import { ReactElement, useState } from "react";

function Github(): ReactElement {
  const [searchInput, setSearchInput] = useState<string>("");
  const { isLoading, data, refetch } = useQuery({
    queryKey: ["github", searchInput],
    queryFn: async () => {
      const response = await fetch(
        `https://api.github.com/search/repositories?q=${searchInput}`,
      );
      return response.json();
    },
    enabled: false,
  });

  function handleSearch(): void {
    refetch();
  }

  return (
    <div>
      <h1>Repositories</h1>
      <input
        type="text"
        value={searchInput}
        onChange={(e) => setSearchInput(e.target.value)}
      />
      <button onClick={handleSearch}>Search</button>
      {isLoading && <p>Loading...</p>}
      <ul>
        {data &&
          Array.isArray(data.items) &&
          data.items.map(
            ({
              id,
              full_name,
              clone_url,
            }: {
              clone_url: string;
              id: string;
              full_name: string;
            }) => (
              <li key={id}>
                <p>{full_name}</p>
                <p>
                  <a href={clone_url} target="_blank" rel="noreferrer">
                    {clone_url}
                  </a>
                </p>
              </li>
            ),
          )}
      </ul>
    </div>
  );
}

export default Github;
