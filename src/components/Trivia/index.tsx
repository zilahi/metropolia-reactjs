import { useQuery } from "@tanstack/react-query";
import { ReactElement } from "react";

function Trivia(): ReactElement {
  const { data, isLoading, refetch } = useQuery({
    queryKey: ["trivia"],
    queryFn: async () => {
      const response = await fetch("https://opentdb.com/api.php?amount=1");
      return response.json();
    },
    enabled: false,
  });
  return (
    <>
      {isLoading && <p>Loading...</p>}
      <button onClick={() => refetch()}>New question</button>
      {data && Array.isArray(data.results) && (
        <p>Question: {data.results[0].question}</p>
      )}
    </>
  );
}

export default Trivia;
