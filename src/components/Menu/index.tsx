import { ReactElement } from "react";

import { menu } from "../../api/menu";

function Menu(): ReactElement {
  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      {menu.menuItems.map(
        ({ name, path }): ReactElement => (
          <a href={path}>{name}</a>
        ),
      )}
    </div>
  );
}

export default Menu;
