import { ReactElement, useState } from "react";

function AgeCheck(): ReactElement {
  const [name, setName] = useState<string>("");
  const [age, setAge] = useState<number>(0);

  function handleClick(): void {
    if (age >= 18) {
      alert(`Hello ${name}`);
    } else {
      alert("You are too young");
    }
  }

  return (
    <>
      <input
        type="text"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      <input
        type="text"
        value={age}
        onChange={(e) => setAge(parseInt(e.target.value))}
      />
      <button onClick={handleClick}>Check age</button>
    </>
  );
}

export default AgeCheck;
