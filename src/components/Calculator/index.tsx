import { ReactElement, useState } from "react";

type OperationType = "add" | "subtract";

function Calculator(): ReactElement {
  const [numA, setNumA] = useState<number>(0);
  const [numB, setNumB] = useState<number>(0);
  const [result, setResult] = useState<number>(0);

  function handleOperation(type: OperationType): void {
    if (type === "add") {
      setResult(numA + numB);
    } else {
      setResult(numA - numB);
    }
  }
  return (
    <>
      <p>Result: {result}</p>
      <input
        type="number"
        value={numA}
        onChange={(e) => setNumA(Number(e.target.value))}
      />
      <input
        type="number"
        value={numB}
        onChange={(e) => setNumB(Number(e.target.value))}
      />
      <button onClick={() => handleOperation("add")}>-</button>
      <button onClick={() => handleOperation("subtract")}>-</button>
    </>
  );
}

export default Calculator;
