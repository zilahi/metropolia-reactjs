interface ITooLong {
  text: string;
}

function TooLong({ text }: ITooLong) {
  return <div>{text.length > 10 ? "Too Long" : text}</div>;
}

export default TooLong;
