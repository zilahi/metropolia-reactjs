import { z } from "zod";
import { ReactElement, useState } from "react";

const User = z.object({
  firstName: z.string().min(1),
  lastName: z.string().min(1),
  phone: z.string().min(1),
  email: z.string().min(1),
});

function RegistrationForm(): ReactElement {
  const [user, setUser] = useState<z.infer<typeof User>>({
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
  });

  const [isValid, setIsValid] = useState<boolean>(false);

  function handleInputValidation(): void {
    try {
      User.parse(user);
      setIsValid(true);
    } catch (error) {
      alert("All fields are required!");
    }
  }
  return (
    <>
      {isValid && (
        <p>
          Welcome {user.firstName} {user.lastName}
        </p>
      )}
      <input
        type="text"
        value={user.firstName}
        onChange={(e) => setUser({ ...user, firstName: e.target.value })}
      />
      <input
        type="text"
        value={user.lastName}
        onChange={(e) => setUser({ ...user, lastName: e.target.value })}
      />
      <input
        type="text"
        value={user.email}
        onChange={(e) => setUser({ ...user, email: e.target.value })}
      />
      <input
        type="text"
        value={user.phone}
        onChange={(e) => setUser({ ...user, phone: e.target.value })}
      />
      <button onClick={(): void => handleInputValidation()}>Submit</button>
    </>
  );
}

export default RegistrationForm;
