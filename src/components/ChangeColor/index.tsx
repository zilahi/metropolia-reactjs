import { ReactElement, useState } from "react";

function ChangeColor(): ReactElement {
  const [color, setColor] = useState<string>("black");
  return (
    <>
      <p
        style={{
          color,
        }}
      >
        Hello World
      </p>
      <button onClick={() => setColor("red")}>Change Color</button>
    </>
  );
}

export default ChangeColor;
