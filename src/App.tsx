import { useState } from "react";
import "./App.css";
import Github from "./components/Github";

function App() {
  const [counter, setCounter] = useState<number>(0);
  return (
    <>
      <div style={{ display: "none" }}>
        <p>counter: {counter}</p>
        <button onClick={() => setCounter((count) => count + 1)}>+</button>
        <button onClick={() => setCounter((count) => count - 1)}>-</button>
        <button onClick={() => setCounter(0)}>Reset</button>
      </div>
      <Github />
    </>
  );
}

export default App;
