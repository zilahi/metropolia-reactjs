import { ReactElement } from "react";
import Menu from "../../components/Menu";
import { Outlet } from "react-router-dom";

function MainLayout(): ReactElement {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
      }}
    >
      <Menu />
      <Outlet />
    </div>
  );
}

export default MainLayout;
